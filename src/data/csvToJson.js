const fs = require('fs');
const path = require('path');
const { parse } = require('csv-parse/sync');


const matchesCsvDataPath = path.join(__dirname, 'matches.csv');
const deliveriesCsvDataPath = path.join(__dirname , 'deliveries.csv');


function data(){

    function matches(){
        const matchesCsv = fs.readFileSync(matchesCsvDataPath , 'utf8');

        const matchesData = parse(matchesCsv, {
          columns: true,
          skip_empty_lines: true,
          relax_column_count: true
        });

        return matchesData;
    }


    function deliveries(){
        const deliveriesCsv = fs.readFileSync(deliveriesCsvDataPath , 'utf8');

        const deliveriesData = parse(deliveriesCsv, {
          columns: true,
          skip_empty_lines: true,
          relax_column_count: true
        });

        return deliveriesData;
    }

    return{
        matches:matches,
        deliveries:deliveries
    }
}

module.exports = data;