function getPlayerWithHighestPomAward(matches) {
    let seasons = {};

    for(let match of matches) {
        let year = match.season;
        let playerName = match.player_of_match;

        if(seasons[year] === undefined) {
            seasons[year] = {}; 
        } if(seasons[year][playerName] === undefined) {
            seasons[year][playerName] = 1;
        } else {
            seasons[year][playerName]++;
        }
    }

    let answerObject = {};
    for(let year in seasons) {
        let players = seasons[year];
        let playerArray = Object.entries(players);

        playerArray.sort((value1, value2) => value2[1] - value1[1]);
        if(answerObject[year] === undefined) {
            answerObject[year] = playerArray[0][0];
        }
    }
    return answerObject;
    
}

module.exports.getPlayerWithHighestPomAward = getPlayerWithHighestPomAward;