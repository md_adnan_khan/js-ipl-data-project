function getStrikeRateOfBatsaManForEachSeason(matches, deliveries) {
    let allId = {};
    let allSeasons = {};

    for(let match of matches) {
        if(allId[match.id] === undefined) {
            allId[match.id] = match.season;
        }
    }
    // console.log(allId);
   
    for(let delivery of deliveries) {
        const year = allId[delivery.match_id];
        let batsman = delivery.batsman;
        let run = parseInt(delivery.batsman_runs);
        let ball = (delivery.wide_runs == 0 && delivery.noball_runs == 0 && delivery.legbye_runs 
            && delivery.bye_runs) ? 1 : 0;
        
        if(allSeasons[year] === undefined) {
            allSeasons[year] = {};
            // console.log(allSeasons[year]);
        } if(allSeasons[year][batsman] === undefined) {
            allSeasons[year][batsman] = {runs: run, balls: 1};
            // console.log(allSeasons);
        } else {
            allSeasons[year][batsman].runs += run;
            allSeasons[year][batsman].balls += ball;
        }
    }
    // console.log(allSeasons);

    let answerObj = {};
    for(let year in allSeasons) {
        for(let batsman in allSeasons[year]) {
            let {runs, balls} = allSeasons[year][batsman];
    
            let strikeRate = (runs / balls) * 100;
            if(answerObj[year] === undefined) {
                answerObj[year] = {};
            } 
            if(answerObj[year][batsman] === undefined) {
                answerObj[year][batsman] = strikeRate;
            } 
        }     
    }
    // console.log(answerObj);
    return answerObj;   
}

module.exports.getStrikeRateOfBatsaManForEachSeason = getStrikeRateOfBatsaManForEachSeason;