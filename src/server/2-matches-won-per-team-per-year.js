function getMatchesWonPerTeamPerYear(matches) {
    let answerObject = {};
    
    // ***Method-1***
    for(let match of matches) {
        let winner = match.winner;
        let winYear = match.season;

        if(answerObject[winYear] === undefined) {
            answerObject[winYear] = {};
        } if(answerObject[winYear][winner] === undefined) {
            answerObject[winYear][winner] = 1;
        } else {
            answerObject[winYear][winner]++;
        }
    }

    // ***Method-2***
    // for(let match of matches) {
    //     let winner = match.winner;
    //     let winYear = match.season;

    //     if(answerObject[winner] === undefined) {
    //         answerObject[winner] = {};
    //     } if(answerObject[winner][winYear] === undefined) {
    //         answerObject[winner][winYear] = 1;
    //     } else {
    //         answerObject[winner][winYear]++;
    //     }
    // }
    // console.log(answerObject);
    
    return answerObject;
}

module.exports.getMatchesWonPerTeamPerYear = getMatchesWonPerTeamPerYear;