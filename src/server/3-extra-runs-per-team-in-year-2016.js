function getExtraRunsPerTeamIn2016(matches, deliveries) {
    let extraRunsObject = {};
    let matchId = {};

    for(let match of matches) {
        if(match.season == 2016) {
            if(matchId[match.id] === undefined) {
                matchId[match.id] = true;
            }
        }
    }

    for(let delivery of deliveries) {
        if(matchId[delivery.match_id] !== undefined) {
            let team = delivery.bowling_team;
            
            if(extraRunsObject[team] === undefined) {
                extraRunsObject[team] = parseInt(delivery.extra_runs);

            } else {
                let extraRun = parseInt(delivery.extra_runs);
                extraRunsObject[team] += extraRun;
            }
        }
    }
    return extraRunsObject;
}

module.exports.getExtraRunsPerTeamIn2016 = getExtraRunsPerTeamIn2016;