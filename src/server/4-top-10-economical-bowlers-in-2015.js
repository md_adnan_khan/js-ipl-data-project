function getTop10EconomicalBowlers(matches, deliveries) {
    let bowlers = {};
    let matchId = {};

    for(let match of matches) {
        if(match.season == 2015) {
            if(matchId[match.id] === undefined) {
                matchId[match.id] = true;
            }
        }
    }

    for(let delivery of deliveries) {

        if(matchId[delivery.match_id] !== undefined) {
            let player = delivery.bowler;
            let totalRun = parseInt(delivery.total_runs);
            let over = (delivery.ball == 1) ? 1 : 0;
            
            if(bowlers[player] === undefined) {
                bowlers[player] = { "runs": totalRun, "overs": 1 };
            } else {
                bowlers[player].overs += over;
                bowlers[player].runs = bowlers[player].runs + totalRun;
            }
        }
    }
    
    let allBowlers = [];
    for(let player in bowlers) {//find the economy of the bowlers
        let {runs, overs} = bowlers[player];
        // console.log(bowlers[player]);

        let economy = runs / overs;
        let name = player;
        allBowlers.push({name, economy});
    }

    allBowlers.sort((bowler1, bowler2) => bowler1.economy - bowler2.economy); //sort them in ascending order
    let top10Bowlers = [];
    let index = 0;

    while(index < 10) { //Get top-10 bowlers
        top10Bowlers.push(allBowlers[index]);
        index++;
    }
    return top10Bowlers;
}

module.exports.getTop10EconomicalBowlers = getTop10EconomicalBowlers;
