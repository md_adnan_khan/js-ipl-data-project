function getTeamsWonTossAndMatch(matches) {
    let teamsWon = {};

    for(let match of matches) {
        let tossWinner = match.toss_winner;
        let matchWinner = match.winner;

        if(tossWinner === matchWinner) {
            if(teamsWon[tossWinner] === undefined) {
                teamsWon[tossWinner] = 1;
            } else {
                teamsWon[tossWinner]++;
            }
        }
    }
    return teamsWon;
}

module.exports.getTeamsWonTossAndMatch = getTeamsWonTossAndMatch;