function getMatchesPerYear(matches, filePath) {
    let matchYearObject = {};

    for(let object of matches) {
        let matchYear = object.season;

        if(matchYearObject[matchYear] === undefined) {
            matchYearObject[matchYear] = 1;
        } else {
            matchYearObject[matchYear]++;
        }
    }
    return matchYearObject;
}

module.exports.getMatchesPerYear = getMatchesPerYear;