function getBowlerWithBestEconomyInSuperOver(deliveries) {
    let bowlers = {};

    for(let delivery of deliveries) {
        if(delivery.is_super_over != "0") {
            let playerName = delivery.bowler;
            let run = parseInt(delivery.total_runs);
            let over = (delivery.ball == 1) ? 1 : 0;

            if(bowlers[playerName] === undefined) {
                bowlers[playerName] = {runs: run, overs: 1};
            } else {
                bowlers[playerName].runs = bowlers[playerName].runs + run;
                bowlers[playerName].overs += over;
            }
        }
    }
    
    let bowlersArray = [];
    for(let player in bowlers) {
        let {runs, overs} = bowlers[player];

        let economy = runs / overs;
        let obj = {player, economy};
        bowlersArray.push(obj);
    }

    bowlersArray.sort((value1, value2) => value1.economy - value2.economy);
    return bowlersArray[0];
}

module.exports.getBowlerWithBestEconomyInSuperOver = getBowlerWithBestEconomyInSuperOver;