function getHighestDismissalByPlayer(matches, deliveries) {
    let bowlers = {};

    for(let delivery of deliveries) {
        let bowler = delivery.bowler;
        let batsman = delivery.batsman;
        let dismissedPlayer = delivery.player_dismissed;

        if(dismissedPlayer != "") {
            if(bowlers[bowler] === undefined) {
                bowlers[bowler] = {};
            } if(bowlers[bowler][batsman] == undefined) {
                bowlers[bowler][batsman] = 0;         
            } 
            bowlers[bowler][batsman]++;
        }
    }

    let answerObj = {};
    let mostDismissal = 0;

    for(let bowler in bowlers) {
        let bowlerObj = bowlers[bowler];

        let bowlersArray = Object.entries(bowlerObj);
        const newArray =  bowlersArray.sort((value1, value2) => value2[1] - value1[1]);

        let playerName = newArray[0][0];
        let dismissal = newArray[0][1];

        if(dismissal > mostDismissal) {
            mostDismissal = dismissal;
            answerObj = {batsman: playerName, total_dismissal: mostDismissal, bowler: bowler};
        }
    }
    
    return answerObj;
}

module.exports.getHighestDismissalByPlayer = getHighestDismissalByPlayer;0