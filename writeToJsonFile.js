const data = require('./src/data/csvToJson.js');
const fs = require('fs');
const path = require('path');

const outputDir = __dirname; 

const matchesData = data().matches();
const deliveriesData = data().deliveries();

function saveJsonToFile(jsonData, outputFileName) {
  const outputFilePath = path.join(outputDir, outputFileName);
  fs.writeFileSync(outputFilePath, JSON.stringify(jsonData, null, 2), 'utf8');
  console.log(`JSON data saved to ${outputFilePath}`);
}

saveJsonToFile(matchesData, 'matches.json');
saveJsonToFile(deliveriesData, 'deliveries.json');