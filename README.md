# IPL Data Project I

This project involves transforming raw IPL data to calculate various statistics. The data is sourced from [Kaggle IPL Dataset](https://www.kaggle.com/datasets/manasgarg/ipl) and consists of two files: `deliveries.csv` and `matches.csv`.


## Objectives

The main goals of this project are to process the IPL data and compute the following statistics:

1. Number of matches played per year for all the years in IPL.
2. Number of matches won per team per year in IPL.
3. Extra runs conceded per team in the year 2016.
4. Top 10 economical bowlers in the year 2015.
5. Number of times each team won the toss and also won the match.
6. Player with the highest number of Player of the Match awards for each season.
7. Strike rate of a batsman for each season.
8. Highest number of times one player has been dismissed by another player.
9. Bowler with the best economy in super overs.

Each task will be implemented as a separate function, and the results will be saved as JSON files in the output folder.

# Getting Started
## Prerequisites
Make sure you have Node.js installed on your machine. You can download it from [Node.js official website](https://nodejs.org/en).

## Installation

***1. Clone the repository:***

```bash
git clone https://gitlab.com/js-ipl-data-project.git
cd js-ipl-data-project
```
**2. Install dependencies:**

```bash
npm install
```

**3. Download the data:**

- Download matches.csv and deliveries.csv from [Kaggle IPL Dataset](https://www.kaggle.com/datasets/manasgarg/ipl).
- Place the downloaded files in the data/ directory.



Make sure you have Node.js installed on your machine. You can download it from

## Project Structure
The project directory structure should look like this:

```kotlin
js-ipl-data-project/
│
├── data/
│   ├── matches.csv
│   └── deliveries.csv
│
├── public/
│   └── output/
│       ├── matchesPerYear.json
│       ├── matchesWonPerTeamPerYear.json
│       ├── extraRunsConceded2016.json
│       ├── top10EconomicalBowlers2015.json
│       ├── tossWinsAndMatches.json
│       ├── playerOfTheMatchAwards.json
│       ├── batsmanStrikeRate.json
│       ├── playerDismissals.json
│       └── bestEconomySuperOvers.json
│
├── src/
│   ├── server/
│   │   ├── 1-matches-per-year.js
│   │   ├── 2-matches-won-per-team-per-year.js
│   │   ├── 3-extra-runs-conceded-2016.js
│   │   ├── 4-top-10-economical-bowlers-2015.js
│   │   ├── 5-toss-wins-and-matches.js
│   │   ├── 6-player-of-the-match-awards.js
│   │   ├── 7-batsman-strike-rate.js
│   │   ├── 8-player-dismissals.js
│   │   └── 9-best-economy-super-overs.js
│   │
├── .gitignore
├── package.json
├── package-lock.json
└── README.md
```

## Running the Project

To run the project and generate the required JSON files, execute the main script. Each module will process the data and output the results to the public/output folder.

## Instructions

***1. Create a GitLab repository:***
- Name the repository `js-ipl-data-project` in the GitLab subgroup.

***2. Follow proper Git practices:***
- Commit frequently with meaningful commit messages.

***3. Ensure all checklist items are covered:***
-Proper Git commits.
- Correct directory structure.
- Updated `package.json` with dependencies and devDependencies.
  `.gitignore file`.
- Use proper and intuitive variable names.
- Separate modules for each function.

## Example Command
To run a specific function, navigate to the src/server directory and execute the respective JavaScript file:

```bash
node src/server/1-matches-per-year.js
```

## Checklist
Before submitting the project, ensure the following:

- Proper Git commits with meaningful messages.
- Correct directory structure as shown above.
- package.json includes all necessary dependencies and devDependencies.
 .gitignore file is present.
- Variables have proper and intuitive names.
- Each function is in a separate module.
  Resources For more details, refer to the JavaScript Full-Stack Path guide.