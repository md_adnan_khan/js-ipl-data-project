const matches = require('./matches.json');
const deliveries = require('./deliveries.json');
const fs = require('fs');
const path = require('path');

//Question-1
let filePath1 = path.join(__dirname, '/src/public/output/matches-won-per-team-per-year.json');
const {getMatchesPerYear} = require('./src/server/1-matches-per-year');

let matchYearObject = getMatchesPerYear(matches);
fs.writeFileSync(filePath1, JSON.stringify(matchYearObject, null, 2));


// //Question-2
let filePath2 = path.join(__dirname, '/src/public/output/matches-won-per-team-per-year.json');
let {getMatchesWonPerTeamPerYear} = require('./src/server/2-matches-won-per-team-per-year');

let answerJsonObject = getMatchesWonPerTeamPerYear(matches);
fs.writeFileSync(filePath2, JSON.stringify(answerJsonObject, null, 2));


// //Question-3
const filePath3 = path.join(__dirname, '/src/public/output/extra-runs-per-team-per-year.json');
let {getExtraRunsPerTeamIn2016}  = require('./src/server/3-extra-runs-per-team-in-year-2016');

let extraRunsObject = getExtraRunsPerTeamIn2016(matches, deliveries);
fs.writeFileSync(filePath3, JSON.stringify(extraRunsObject, null, 2));

//Question-4
const filePath4 = path.join(__dirname, '/src/public/output/top-10-economical-bowlers-in-2015.json');
let {getTop10EconomicalBowlers}  = require('./src/server/4-top-10-economical-bowlers-in-2015');

let top10Bowler = getTop10EconomicalBowlers(matches, deliveries);
fs.writeFileSync(filePath4, JSON.stringify(top10Bowler, null, 2));


//Question-5
const filePath5 = path.join(__dirname, 'src/public/output/teams-who-won-toss-and-match.json');
let {getTeamsWonTossAndMatch}  = require('./src/server/5-find-teams-who-won-toss-and-match');

let teamsWonTossAndMatch = getTeamsWonTossAndMatch(matches);
fs.writeFileSync(filePath5, JSON.stringify(teamsWonTossAndMatch, null, 2));

// Question-6
const filePath6 = path.join(__dirname, 'src/public/output/highest-POM-award.json');
let {getPlayerWithHighestPomAward}  = require('./src/server/6-highest-POM-award.js');

let playerWithHighestAward = getPlayerWithHighestPomAward(matches);
fs.writeFileSync(filePath6, JSON.stringify(playerWithHighestAward, null, 2));

// //Question-7
const filePath7 = path.join(__dirname, 'src/public/output/batsman-strike-rate.json');
let {getStrikeRateOfBatsaManForEachSeason}  = require('./src/server/7-batsman-strike-rate.js');

let strikeRateOfBatsman = getStrikeRateOfBatsaManForEachSeason(matches, deliveries);
fs.writeFileSync(filePath7, JSON.stringify(strikeRateOfBatsman, null, 2));


//Question-8
const filePath8 = path.join(__dirname, 'src/public/output/highest-dismissed-player.json');
let {getHighestDismissalByPlayer}  = require('./src/server/8-get-dismissed-player.js');

let highestDismissal = getHighestDismissalByPlayer(matches, deliveries);
fs.writeFileSync(filePath8, JSON.stringify(highestDismissal, null, 2));

//Question-9
const filePath9 = path.join(__dirname, 'src/public/output/bowlers-best-eco-in-season.json');
let {getBowlerWithBestEconomyInSuperOver}  = require('./src/server/9-best-economy-bowler-in-super-overs.js');

let bestEconomy = getBowlerWithBestEconomyInSuperOver(deliveries);
fs.writeFileSync(filePath9, JSON.stringify(bestEconomy, null, 2));